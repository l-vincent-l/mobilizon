export interface IAddress {
  description: string;
  floor: string;
  street: string;
  locality: string;
  postal_code: string;
  region: string;
  country: string;
  geom: string;
}
